package com.zero2oneit.mall.member.api;


import com.zero2oneit.mall.common.annotion.RepeatSubmit;
import com.zero2oneit.mall.common.bean.auth.AppWxLoginObject;
import com.zero2oneit.mall.common.bean.auth.RegisterInfo;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.member.service.MemberInfoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Title: LoginApi
 * @Description:
 * @Auther:zhj
 * @Version: 1.0
 * @create 2020/7/22 16:00
 */
@RestController
@RequestMapping("/login/memberApp")
public class LoginApi {

    @Autowired
    MemberInfoService memberInfoService;


    /**
     * 微信小程序授权登录
     * @param
     * @return
     */
    @ApiOperation("微信小程序授权登录")
    @PostMapping("/appeltWxauth")
    public R appeltWxauth(@RequestBody AppWxLoginObject appWxLoginObject){

        return memberInfoService.appeltWxauth(appWxLoginObject);
    }



    /**
     * app微信授权登录
     * @param
     * @return
     */
    @ApiOperation("app微信授权登录")
    @PostMapping("/wxauth")
    public R wxauth(@RequestBody AppWxLoginObject appWxLoginObject){

        return memberInfoService.wxauth(appWxLoginObject);
    }


    @ApiOperation("手机号码验证码登录")
    @RequestMapping("/appCodePhone")
    public R appCodePhone(@RequestBody AppWxLoginObject appWxLoginObject){
        return memberInfoService.appCodePhone(appWxLoginObject);
    }

    @ApiOperation("密码账号登录")
    @RequestMapping("/appPassPhone")
    public R appPassPhone(@RequestBody AppWxLoginObject appWxLoginObject){
        return memberInfoService.appPassPhone(appWxLoginObject);
    }

    @ApiOperation("微信绑定手机号码")
    @RequestMapping("/bindWx")
    public R bindWx(@RequestBody AppWxLoginObject appWxLoginObject){
        return memberInfoService.bindWx(appWxLoginObject);
    }

    @ApiOperation("检查号码是否存在")
    @RequestMapping("/checkPhone")
    public Boolean checkPhone(@RequestBody String  phone){
        return memberInfoService.checkPhone(phone);
    }

    @ApiOperation("注册")
    @RequestMapping("/register")
    @RepeatSubmit
    public R register(@RequestBody AppWxLoginObject appWxLoginObject){
        return memberInfoService.register(appWxLoginObject);
    }

    @ApiOperation("修改密码")
    @RequestMapping("/appUpdatePass")
    R appUpdatePass(@RequestBody AppWxLoginObject appWxLoginObject){
        return memberInfoService.appUpdatePass(appWxLoginObject);
    }

    @ApiOperation("网页注册")
    @RequestMapping("/registerHtml")
    @RepeatSubmit
    public R registerHtml(@RequestBody RegisterInfo registerInfo){
        return memberInfoService.registerHtml(registerInfo);
    }

}
