package com.zero2oneit.mall.member.feign;

import com.zero2oneit.mall.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * Description:
 *
 * @author leon
 * @date 2020/12/3 10:56
 */
@FeignClient(name ="coupon-service")
public interface CouponFeign {
    /**
     *查询自己优惠券总数 String memberId
     */
    @PostMapping("/api/couponapi/selectCountByMemberId")
    public R selectCountByMemberId(@RequestBody String memberId);
}
