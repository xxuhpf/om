package com.zero2oneit.mall.member.api;

import com.zero2oneit.mall.common.bean.auth.WxAppletsAuth;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.member.service.MemberLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description:
 *
 * @author yjj
 * @date 2021/3/5 18:18
 */
@RestController
@RequestMapping("/member/login")
public class MemberLoginApi {

    @Autowired
    private MemberLoginService memberLoginService;

    /**
     * 微信小程序登录
     * @param wxAppletsAuth
     * @return
     */
    @PostMapping("/WxAppletsLogin")
    public R WxAppletsLogin(@RequestBody WxAppletsAuth wxAppletsAuth){
        return memberLoginService.WxAppletsLogin(wxAppletsAuth);
    }
}
