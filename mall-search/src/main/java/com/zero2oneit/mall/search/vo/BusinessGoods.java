package com.zero2oneit.mall.search.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * Description:
 *
 * @author Lee
 * @date 2021/3/17 12:08
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BusinessGoods {
    private String businessId;
    private String businessName;
    private Map businessGoods;
}
