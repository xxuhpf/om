package com.zero2oneit.mall.common.query.member;

import com.zero2oneit.mall.common.utils.query.QueryObject;
import lombok.Data;

/**
 * Description:
 *
 * @author yjj
 * @date 2020/7/16 11:09
 */
@Data
public class InfoQueryObject extends QueryObject {

    //会员id
    private String memberId;

    //会员名称
    private String memberName;

    //联系电话
    private String memberPhone;

    //会员等级
    private String gradeId;

}
