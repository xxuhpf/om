package com.zero2oneit.mall.common.bean.member;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Description:
 *
 * @author yjj
 * @date 2020/7/15 20:06
 */
@Data
@TableName(value = "member_info")
public class MemberInfo implements Serializable {
    @TableId(value = "member_id", type = IdType.ID_WORKER_STR)
    private String memberId;
    private String memberAccount;
    private String memberPassword;
    private String nickName;
    private String wxOpenid;
    private String wxApplteOpenid;
    private String memberName;
    private String memberAvatar;
    private String memberGrade;
    private Integer sex;
    private String payPwd;
    private String memberPhone;
    private String memberEmail;
    private String address;
    private Date birthday;
    private Date registerTime;
    @TableField(strategy = FieldStrategy.IGNORED)
    private String stationId;
    private Integer gradeId;
    private String userQrCode;
    @TableField(strategy = FieldStrategy.IGNORED)
    private Integer bypassAccount;//1是子账号，0是主账号

    private Integer starId;
    private Integer starOpen;
}
